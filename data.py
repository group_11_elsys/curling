import matplotlib.pyplot as plt
from scipy.signal import bspline, cubic, quadratic,savgol_filter
import numpy as np
from scipy import signal
import math
from numpy import diff
from colour import Color


abs_acc=[]
xdata_true=[]
ydata_true=[]
zdata_true=[]

xdata_temp_str=[]
ydata_temp_str=[]
zdata_temp_str=[]

xdata_temp_int=[]
ydata_temp_int=[]
zdata_temp_int=[]
files=["video4.txt"] # 8,3,4,5,6,7
for filename in files:
    f = open(filename, "r")
    Lines = f.readlines()
    xdata_temp_str=Lines[0].split(",")
    for i in range(len(xdata_temp_str)):
        xdata_temp_int.append(int(xdata_temp_str[i].strip(), 16)*0.00059857*3.8)
    ydata_temp_str=Lines[1].split(",")
    for i in range(len(xdata_temp_str)):
        ydata_temp_int.append(int(ydata_temp_str[i].strip(), 16)*0.00059857*3.8)
    zdata_temp_str=Lines[2].split(",")
    for i in range(len(xdata_temp_str)):
        zdata_temp_int.append(int(zdata_temp_str[i].strip(), 16)*0.00059857*3.8)
    xdata_true.append(xdata_temp_int)
    ydata_true.append(ydata_temp_int)
    zdata_true.append(zdata_temp_int)
    f.close()

# print(xdata_true)
# print(ydata_true)
# print(zdata_true)

# print(len(xdata_true))
# print(len(ydata_true))
# print(len(zdata_true))

# print(len(xdata_true[0]))
# print(len(ydata_true[0]))
# print(len(zdata_true[0]))

abs_acc_temp=[]
# print(xdata_true[0][1])
for i in range(len(xdata_true)):
    for j in range(len(xdata_true[i])):
        abs_acc_temp.append(math.sqrt(xdata_true[i][j]*xdata_true[i][j]+ydata_true[i][j]*ydata_true[i][j]+zdata_true[i][j]*zdata_true[i][j]))
    abs_acc.append(abs_acc_temp)
# print(abs_acc[0])

# dx = 1
# dy = diff(abs_acc[0])/dx

xdata_vec=0
ydata_vec=0
zdata_vec=0
acc_vec_arr=[]
for i in range(0,len(xdata_true[0])-1):
    xdata_vec+=xdata_true[0][i]
    ydata_vec+=ydata_true[0][i]
    zdata_vec+=zdata_true[0][i]

    # xdata_vec+=xdata_true[i]*pow(dy_norm[i], 2/3)
    # ydata_vec+=ydata_true[i]*pow(dy_norm[i], 2/3)
    # zdata_vec+=z6data_true[i]*pow(dy_norm[i], 2/3)
    acc_vec_arr.append([xdata_vec, ydata_vec,zdata_vec])
abs_vec=math.sqrt(xdata_vec*xdata_vec+ydata_vec*ydata_vec+zdata_vec*zdata_vec)
# acc_unit_vec=[xdata_vec/abs_vec,ydata_vec/abs_vec,zdata_vec/abs_vec]

red = Color("blue")
colors = list(red.range_to(Color("red"),360))

fig3 = plt.figure()
ax3 = fig3.add_subplot(projection='3d')

fig4= plt.figure()
ax41 = fig4.add_subplot()


vel_vec_arr=[[0,0,0]]
vel_arr=[]
k=1
ax3.scatter(vel_vec_arr[0][0], vel_vec_arr[0][1], vel_vec_arr[0][2], color=colors[i].hex)
for i in range(0, 349): # 
    vel_vec_arr.append([vel_vec_arr[k-1][0]+xdata_true[0][i], vel_vec_arr[k-1][1]+ydata_true[0][i], vel_vec_arr[k-1][2]+zdata_true[0][i]])
    # vel_arr.append(np.sqrt(vel_vec_arr[i][0]**2+vel_vec_arr[i][1]**2+vel_vec_arr[i][2]**2))
    ax3.scatter(abs_acc[0][k], abs_acc[0][k], abs_acc[0][k], color=colors[i].hex)
    ax41.grid()
    ax41.scatter(i, vel_vec_arr[k][0], color=Color("red").hex)
    ax41.scatter(i, vel_vec_arr[k][1] , color=Color("green").hex)
    ax41.scatter(i,vel_vec_arr[k][2], color=Color("blue").hex)
    # ax3.scatter(acc_vec_arr[i][0], acc_vec_arr[i][1], acc_vec_arr[i][2], color=colors[i].hex)
    k+=1




# ax3.scatter(acc_unit_vec[0], acc_unit_vec[1], acc_unit_vec[2], color="blue")
ax3.set_xlabel('X Label')
ax3.set_ylabel('Y Label')
ax3.set_zlabel('Z Label')


fig = plt.figure()
ax11 = fig.add_subplot()
ax12 = fig.add_subplot()

figure=plt.figure()
ax21 = figure.add_subplot()
ax22 = figure.add_subplot()


abs_acc_np = np.array(abs_acc[0])
peaks_sci= signal.find_peaks(abs_acc_np)
# print(peaks_sci)
peaks=[]
for el in peaks_sci[0]:
    peaks.append(el)


time=[]
for i  in range(0,349):
    time.append(i)
ax11.plot(abs_acc_np)
# ax12.plot(time, vel_arr)

ax11.grid()
# ax12.plot(peaks,abs_acc_np[peaks], "x")
results_half = signal.peak_widths(abs_acc_np, peaks, rel_height=0.5)
results_full=signal.peak_widths(abs_acc_np, peaks, rel_height=1) 
prominences = signal.peak_prominences(abs_acc_np, peaks)[0]
contour_heights = abs_acc_np[peaks] - prominences
minimas=signal.argrelmin(abs_acc_np)
# print(minimas[0])

# ploter peaks
for el in minimas[0]:
    ax12.scatter(el, abs_acc_np[el])
# print(results_full[0])  # widths
for el in peaks_sci[0]:
    ax12.scatter(el, abs_acc_np[el])
    ax12.hlines(*results_half[1:], color="C2")
    ax12.hlines(*results_full[1:], color="C3")
    ax12.vlines(x=peaks, ymin=contour_heights, ymax=abs_acc_np[peaks])

# figure=plt.figure()
# abs_acc_butt=signal.filtfilt(butt1,butt2, abs_acc[0])
# ax21.plot(abs_acc_butt)
# ax22.plot(signal.savgol_filter(abs_acc[0],15,3))


ax22.grid()
abs_vec_savgol=signal.savgol_filter(abs_acc[0],15,3)
for i in range(0, len(xdata_true[0])):
    ax22.scatter(i,abs_acc[0][i],color=colors[i].hex)


sum_vel=0
sum_vel_savgol=0
var=[abs_acc[0][0]]
for i in range(1,350):
    if i>=64 and i<=101:
        var.append(var[i-1]+abs_acc[0][i]*0.016)
    else:
        var.append(0)
ax12.plot(var)

abs_test=[0.0476270147344161
,	0.135718919291658
,	0.0422359357743816
,	0.635990474557892
,	0.529128921286
,	0.7535288245681
,	0.477859681166859
,	0.924094474677011
,	0.889323360298732
,	0.0554644083494343
,	1.62626226603993
,	0.864810985761445
,	0.748538581274733
,	1.60260584809831
,	0.0936153092814397
,	1.65772112971529
,	2.07203154869991
,	2.33113084445085
,	3.19038608723997
,	5.05893938629674
,	5.84389545034681
,	5.42704734349042
,	4.79738919667174
,	5.08053680235528
,	4.55024678365146
,	5.19428745284432
,	6.8139484891561
,	5.51640672521659
,	3.75749620652145
,	5.47869320042686
,	7.27588124849388
,	4.96049445070764
,	4.94504452104784
,	5.85126432955582
,	3.4759771023661
,	3.1118059629246
,	2.59305034572601
,	6.56663581133995
,	5.99171366589164
,	5.7206606364694
,	1.80707745051087
,	3.95346308755798
,	3.95147125996414
,	2.82925966914347
,	2.66382747084678
,	5.04642190721919
,	5.50582005697749
,	4.60640007764561
,	2.16999450102467
,	4.30465515632368
,	1.25427823439279
,	1.88507636391053
,	3.03876875252944
,	4.46448528062121
,	0.364285198398117
,	5.30579385140362
,	8.45862345173454
,	17.3964104489678
,	20.1067617611889
,	23.0710604914091
,	21.9322603141216
,	26.8075951986056
,	15.7411785005812
,	6.00857358176908
,	0.974880603183193]

test_fig = plt.figure()
ax1_test = test_fig.add_subplot()
ax2_test = test_fig.add_subplot()

ax1_test.plot(abs_test)
ax1_test.grid()


# var_savgol=[0]
# for i in range(1,111):
#     if i>=69:
#         var_savgol.append(var[i-1]+abs_vec_savgol[i]*0.016)
#     else:
#         var_savgol.append(0)
# ax12.plot(var_savgol)



# np.trapz(var,dx=0.016)
# np.trapz(sum_vel_savgol[69:111],dx<=0.016)
#     # if abs_acc[0][i]>=2.5:
#     sum_vel+=abs_acc[0][i]*0.016
#     # if abs_vec_savgol[i]>=2.5:
#     sum_vel_savgol+=abs_vec_savgol[i]*0.016
print(sum_vel)
print(sum_vel_savgol)

plt.show()
# # vel=[]
# # for i in range(len(abs_acc1)):
# #     if i!=0:
# #        vel.append(vel[i-1]+abs_acc1[i]*0.0166)  
# #     else:
# #        vel.append(abs_acc1[i]*0.0166)  

# # fig5=plt.figure()
# # ax5 = fig5.add_subplot()
# # abs_acc1=signal.savgol_filter(abs_acc1,15,5)
# # ax5.plot(abs_acc1)
# # ax5.grid()

# # fig6=plt.figure()
# # ax6 = fig6.add_subplot()
# # abs_acc1=signal.savgol_filter(vel,15,5)
# # ax6.plot(vel)
# # ax6.grid()







# dy_abs=np.absolute(np.array(dy))
# dy_nrm = np.linalg.norm(dy_abs)
# dy_savgol=signal.savgol_filter(dy,13,3)
# # dy_savgol_abs=np.absolute(np.array(dy_savgol))
# # savgol_nrm = np.linalg.norm(dy_savgol_abs)
# # normal_dy = dy_abs/dy_nrm
# # normal_savgol = dy_savgol_abs/savgol_nrm
# # ax11.plot([i for i in range(0,349)], normal_dy)



# f = open("xdata_true_Kopi2.txt", "r")
# read_content = f.read()
# xdata_true=read_content.split(",")
# for i in range(len(xdata_true)):
#     xdata_true[i]=int(xdata_true[i].strip(), 16)*0.00059857*3
# f.close()
# # print(xdata_true)

# f = open("ydata_true_Kopi2.txt", "r")
# read_content = f.read()
# ydata_true=read_content.split(",")
# for i in range(len(ydata_true)):
#     ydata_true[i]=int(ydata_true[i].strip(), 16)*0.00059857*3
# f.close()
# # print(ydata_true)

# f = open("zdata_true_Kopi2.txt", "r")
# read_content = f.read()
# zdata_true=read_content.split(",")
# for i in range(len(zdata_true)):
#     zdata_true[i]=int(zdata_true[i].strip(), 16)*0.00059857*3
# f.close()
# # print(zdata_true)


# print(np.trapz(abs_acc, dx=0.016))



# xdata_vec=0
# ydata_vec=0
# zdata_vec=0
# acc_vec_arr=[]
# for i in range(0,len(xdata_true)-1):
#     xdata_vec+=xdata_true[i]
#     ydata_vec+=ydata_true[i]
#     zdata_vec+=zdata_true[i]

#     # xdata_vec+=xdata_true[i]*pow(dy_norm[i], 2/3)
#     # ydata_vec+=ydata_true[i]*pow(dy_norm[i], 2/3)
#     # zdata_vec+=zdata_true[i]*pow(dy_norm[i], 2/3)
#     acc_vec_arr.append([xdata_vec, ydata_vec,zdata_vec])
# abs_vec=math.sqrt(xdata_vec*xdata_vec+ydata_vec*ydata_vec+zdata_vec*zdata_vec)
# acc_unit_vec=[xdata_vec/abs_vec,ydata_vec/abs_vec,zdata_vec/abs_vec]
# abs_acc=signal.savgol_filter(abs_acc,21,3)


# dy_abs=np.absolute(np.array(dy))
# dy_max=np.amax(dy_abs)
# dy_norm=dy_abs*(1/dy_max)


# print("ønsket: ")
# print(1)
# print(np.arctan(math.sqrt(23**2+8.5**2)/25))
# print(np.arctan(8.5/23))


# print("Fra beregninger: ")
# print(math.sqrt(acc_unit_vec[0]**2+acc_unit_vec[1]**2+acc_unit_vec[2]**2))
# print(np.arctan(math.sqrt(acc_unit_vec[0]**2+acc_unit_vec[1]**2)/acc_unit_vec[2]))
# print(np.arctan(acc_unit_vec[1]/acc_unit_vec[0]))
# # print(acc_vec_arr)


# red = Color("blue")
# colors = list(red.range_to(Color("red"),360))
# # print(colors[1].hex)




# fig = plt.figure()
# ax3 = fig.add_subplot(projection='3d')

# # COLOR='blue'
# # RESFACT=10
# # MAP='winter' # choose carefully, or color transitions will not appear smoooth
# # cm = plt.cm.get_cmap(MAP)
# # ax1 = fig.add_subplot(221) # regular resolution color map
# # ax2.set_prop_cycle(color=[x.hex for x in colors])

# # for i in range(len(abs_acc)):
#     # ax1.plot(i,abs_acc[i])
# # print([x.hex for x in colors])


# for i in range(len(acc_vec_arr)):
#     ax2.scatter(i,abs_acc[i],color=colors[i].hex)
#     ax3.scatter(acc_vec_arr[i][0], acc_vec_arr[i][1], acc_vec_arr[i][2], color=colors[i].hex)
# ax3.scatter(acc_unit_vec[0]*10, acc_unit_vec[1]*10, acc_unit_vec[2]*10, color="blue")

# ax3.set_xlabel('X Label')
# ax3.set_ylabel('Y Label')
# ax3.set_zlabel('Z Label')

# # f = open("noisy_test_data.txt", "r")
# # read_content = f.readlines()
# # abs_acc1=[]
# # # abs_acc2=[]
# # for i in range(len(read_content)):
# #     abs_acc1.append(float(read_content[i].strip()))
# #     # abs_acc2.append(float(read_content[i].strip()))
# # f.close()







# # spline_cub=bspline(abs_acc, 10)
# # print(spline_cub)
# # print(xdata_true)
# # np.set_printoptions(precision=2)
# # abs_acc_np=np.array(abs_acc)
# # abs_acc_savgol=savgol_filter(abs_acc_np, 9, 5)
# # abs_acc_resample = signal.resample(np.array(abs_acc), 500) # kan brukes hvis ikke nok data
# # abs_acc_resample=signal.resample_poly(abs_acc, 5, 2)
# # plt.plot(abs_acc)
# # plt.ylabel('Acceleration')
# # plt.show()
# # plt.plot(abs_acc_resample)
# # plt.ylabel('Acceleration')
# # plt.show()
