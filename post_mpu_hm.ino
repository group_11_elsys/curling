/*===============KOBLINGSSKJEMA=========================
INT til pin 25
SDA til pin 21
SCL til pin 22
GND til GND
VCC til 3V3

Knapp til pin 19 
++ https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button

*/
//Biblioteker for å koble ESP32 til WiFi
#include <WiFi.h>        
#include <DNSServer.h>
#include <WiFiClient.h> 
#include <HTTPClient.h>

//Biblioteker for å bruke I^2C-bussen og styre MPU6050
#include "MPU6050_6Axis_MotionApps20.h"
#include <Wire.h>


// class default I2C addresse er 0x68, så vi legger ikke inn noen argumenter her.
MPU6050 mpu;




// MPU control/status vars: Disse styrer interrupt funksjonen til MPU6050
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars: Disse brukes til å regne ut akselerasjonen til systemet
// korrigert for dets orientering i rommet og tyngdeakselerasjonen
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector


//Pins for å styre knapper og MPU6050
#define INTERRUPT_PIN 25
#define BUTTON_PIN 19
//#define ERROR_PIN 13
//#define WAIT_PIN 14
#define READY_PIN 15
#define SUCCESS_PIN 16

const char DEVICE_NAME[] = "mpu6050";


//Variabler som lagrer dataen vi trenger for å sende POST-request til databasen
String post_data = "";
bool first_connect = false;
String cookies = "";
String x_CSRFToken="";



//===============WIFI NAVN OG PASSORD=================== 
const char* ssid = "Galaxy A516052";
const char* password = "mjsm4054";

//Feltene i vår POST-request
const char * headerKeysPOST[] = {"xdata", "ydata", "zdata"};
const char * headerKeysGET[] = {"Set-Cookie"};
const size_t numberOfHeadersPOST = sizeof(headerKeysPOST)/sizeof(char*);
const size_t numberOfHeadersGET = sizeof(headerKeysGET)/sizeof(char*);

//===========LISTER FOR POST DATA===========
int x_acc[400];
int y_acc[400];
int z_acc[400];
String sensorData[3]; //x, y, z, tid
int num_of_measurements = 0;


bool button_pushed = false; //Variabel som brukes til å bestemme når vi skal sende data
volatile bool sendPOST = false; //Variabel som bestemmer om vi har sendt POST-request og derfor er klare til å samle inn data igjen

//Bool som brukes til å generere interrupts
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void ICACHE_RAM_ATTR dmpDataReady() {
  mpuInterrupt = true;
}


//Denne funksjonen setter opp MPU slik vi vil
void mpu_setup() {
  // Her sjekker vi om byggemiljøet vårt har fast-Wire
  //i så fall trenger vi ikke tvinge mikrokontrolleren til å bruke pin 21 til SDA og pin 22 SCL
  //siden dette er default-verdier i bibioteket "MPU6050_6Axis_MotionApps20.h"
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin(21 , 22, 400000);
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
  #endif

  // initialiserer med funksjon og setter opp at vår pin 25 skal ta imot interrupts fra MPU6050
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);

  // Sjekker at MPU6050 og ESP32 kommuniserer på riktig måte
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // Laster inn Digital Motion Processor på MPU6050 
  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();


  mpu.setRate(7); //Velger antall målinger per sekund, her 62,5 Hz
  //Disse funksjonene kalibrerer så lenge de trenger, bruker kortere tid dersom du holder systemet i ro
  mpu.CalibrateGyro(); //Kalibrerer gyroskop
  mpu.CalibrateAccel(); //Kalibrerer akselerometer
  mpu.PrintActiveOffsets(); //Skriver ut hvilke offsets kalibrering har gitt oss for debugging

  // Starter kun opp MPU dersom den er koblet til riktig
  if (devStatus == 0) {

    // turn on the DMP, now that it's ready
    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // Legger til interruptet på riktig måte
    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
}

//Her kobler vi oss til WiFi
void setup() {
  Serial.begin(115200);
  delay(4000);
//Logger inn på WiFi-nettverk med navn ssid og passord password
  WiFi.begin(ssid, password);
  
  //Venter med å gå videre i koden til vi er koblet til WiFi
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  //Kjører funksjonen som setter opp MPU6050
  mpu_setup();

  //Gjør pins klare til å ta imot knappesignal og gi lys på dioder
  pinMode(BUTTON_PIN, INPUT);
  pinMode(READY_PIN, OUTPUT);
  pinMode(SUCCESS_PIN, OUTPUT);
} 


//Funksjonen som sjekker om vi har fått interrupt fra MPU6050
//Essensielt sjekker denne funksjonen om vår DMP har gjort klar alle data, og så leser vi dem inn
void mpu_loop() {
  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // wait for MPU interrupt or extra packet(s) available
  if (!mpuInterrupt && fifoCount < packetSize) return;

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;

    // display initial world-frame acceleration, adjusted to remove gravity
    // and rotated based on known orientation from quaternion
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);


    //Dersom knappen er trykt inn skal vi samle inn data i x, y og z-retning
    if(button_pushed){
      Serial.print("Knapp er trykt");
      x_acc[num_of_measurements] = aaWorld.x;
      y_acc[num_of_measurements] = aaWorld.y;
      z_acc[num_of_measurements] = aaWorld.z;
      num_of_measurements += 1;
      Serial.println(num_of_measurements);
     }
  }
}

//Vår main-funksjon
void loop() {
 

 HTTPClient http; 
 http.collectHeaders(headerKeysGET, numberOfHeadersGET); 
 // Your Domain name with URL path or IP address with path
 http.begin("https://gruppe11.innovasjon.ed.ntnu.no/nettside_app/sensor/");

  //Kobler oss til databasen
  if (!first_connect){
    int httpCode = http.GET();
    //Dersom vi har klart å få cookies kjøres denne koden
    if (httpCode == 200) { //Check for the returning code
 
        //String payload = http.getString();
        //Serial.println(httpCode);
        //Serial.println(payload);

        //=========X-CSRFTOKEN==================
        //cookies += String(http.header("Cookies"));
        
         for(int i = 0 ; i < http.headers() ; i++){
          cookies=http.header(i);
         } 
        
        //Sier om vi har fått riktig cookies fra nettsiden vår
        Serial.print("cookies check:");
        Serial.println(cookies);
        x_CSRFToken=cookies.substring(10, 74);
        cookies=cookies.substring(0, 74);
        Serial.print("X-CSRFToken check:");
        Serial.println(x_CSRFToken);
        Serial.print("cookies check:");
        Serial.println(cookies);
       
      } else {
        Serial.println("Error on HTTP request");
      }  
      first_connect = true;
    }

  //======================KJØRER MPU=====================
  mpu_loop();


  if (digitalRead(BUTTON_PIN)==HIGH) {
    delay(2000);
    digitalWrite(READY_PIN, HIGH);
    button_pushed=true; // kalibrering her
  }
  //============HER VELGER DU ANTALL MÅLINGER
  if(num_of_measurements == 350 && !sendPOST){
    sensorData[0]="";
    sensorData[1]="";
    sensorData[2]="";    
    sensorData[3]="";

    //Her legges dataene våre inn i meldingen som sendes til databasen
    for(int i = 0; i < num_of_measurements-1; i++){
      sensorData[3]+=String(time_since_start[i]) + ",";
      
      if (x_acc[i]<0) {
        sensorData[0] += "-"+ String((-1)*x_acc[i], HEX) +",";
      } else {
        sensorData[0] += String(x_acc[i], HEX) +",";
      } 

      if (y_acc[i]<0) {
        sensorData[1] += "-"+ String((-1)*y_acc[i], HEX) +",";
      } else {
        sensorData[1] += String(y_acc[i], HEX) +",";
      } 

      if (z_acc[i]<0) {
        sensorData[2] += "-"+ String((-1)*z_acc[i], HEX) +",";
      } else {
        sensorData[2] += String(z_acc[i], HEX) +",";
      } 

     }


    if (x_acc[num_of_measurements-1]<0) {
      sensorData[0] += "-"+ String((-1)*x_acc[num_of_measurements-1], HEX);
    } else {
      sensorData[0] += String(x_acc[num_of_measurements-1], HEX);
    } 

    if (y_acc[num_of_measurements-1]<0) {
      sensorData[1] += "-"+ String((-1)*y_acc[num_of_measurements-1], HEX);
    } else {
      sensorData[1] += String(y_acc[num_of_measurements-1], HEX);
    } 

    if (z_acc[num_of_measurements-1]<0) {
      sensorData[2] += "-"+ String((-1)*z_acc[num_of_measurements-1], HEX);
    } else {
      sensorData[2] += String(z_acc[num_of_measurements-1], HEX);
    } 

    
     num_of_measurements=0;//Setter denne variabel til null slik at vi kan samle inn 350 datapunkter igjen

     //Kode til å sjekke om vi har lagt inn data på riktig måte i HTTP-melding til database
     //Serial.print("DATA CHECK:");
     //Serial.print(sensorData[0]);
     //Serial.print("&&");
     //Serial.print(sensorData[1]);
     //Serial.print("&&");
     //Serial.println(sensorData[2]);
     //Nå skal vi sende POST-request
     sendPOST = true;
     button_pushed=false; //nå kan vi trykke på knappen igjen for å samle inn data fra nytt skyv
   }

  //Her sender vi POST-request
  if (sendPOST){
    String postData="";
    

    //Legger til dataene våre i samme datafelt      
    for ( int fieldNumber = 0; fieldNumber < numberOfHeadersPOST-1; fieldNumber++ ){
        //Serial.print("fieldNumber: ");
        //Serial.println(fieldNumber);
        String fieldName = headerKeysPOST[fieldNumber];
        String fieldData =  sensorData[fieldNumber];
        //Serial.print("fieldName: ");
        //Serial.println(fieldName);
        //Serial.print("fieldData: ");
        //Serial.println(fieldData);
  
        postData += fieldName + "=" + fieldData;
        postData += "&";
     }

      String fieldName = headerKeysPOST[numberOfHeadersPOST-1];
      String fieldData =  sensorData[numberOfHeadersPOST-1];
      //Serial.print("fieldName: ");
      //Serial.println(fieldName);
      //Serial.print("fieldData: ");
      //Serial.println(fieldData);

      postData += fieldName + "=" + fieldData;
      Serial.println(postData);


    //=============HER SENDER VI POST==================
    http.addHeader("Content-Type", "application/json");
    http.addHeader("X-CSRFToken", x_CSRFToken); 
    http.addHeader("referer", "https://gruppe11.innovasjon.ed.ntnu.no");
    http.addHeader("Cookie", cookies);

    
    //int httpCode = http.POST(postData);
    //int httpResponseCode = http.POST(postTime);
    //Serial.print("httpResponseCode: ");  
    //Serial.println(httpResponseCode); 
    
    int httpResponseCode = http.POST(postData);
    Serial.print("httpResponseCode: ");  
    Serial.println(httpResponseCode); 

    //Setter lys slik at bruker kan se at data har blitt sendt til nettside. 
    digitalWrite(READY_PIN, LOW);
    digitalWrite(SUCCESS_PIN, HIGH);
    //Nå skal vi ikke lenger sende POST-request, men vente på nytt knappetrykk, derfor settes disse boolene slik
     sendPOST = false;
     button_pushed = false;
  }
}
