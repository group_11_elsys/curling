/*===============KOBLINGSSKJEMA=========================
INT til pin 25
SDA til pin 21
SCL til pin 22
GND til GND
VCC til 3V3

Knapp til pin 19 
++ https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button

*/

#include <WiFi.h>      //ESP32 Core WiFi Library    
#include <DNSServer.h>
#include <WiFiClient.h> 
#include <HTTPClient.h>

#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"


// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high



// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector

#define INTERRUPT_PIN 25
#define BUTTON_PIN 19

const char DEVICE_NAME[] = "mpu6050";

String post_data = "";
bool first_connect = false;
String cookies = "";
String x_CSRFToken="";



//===============WIFI NAVN OG PASSORD=================== 
const char* ssid = "ONEPLUS_co_apwshm";
const char* password = "wyry9978";

//const char * headerKeysPOST[] = {"x-akse","y-akse","z-akse", "tid"};
const char * headerKeysPOST[] = {"xdata", "ydata", "zdata"};
const char * headerKeysGET[] = {"Set-Cookie"};
const size_t numberOfHeadersPOST = sizeof(headerKeysPOST)/sizeof(char*);
const size_t numberOfHeadersGET = sizeof(headerKeysGET)/sizeof(char*);

//===========LISTE FOR POST DATA===========
int x_acc[1000];
int y_acc[1000];
int z_acc[1000];
int time_since_start[1000];
String sensorData[4]; //x, y, z, tid
int start_time;
int num_of_measurements = 0;
bool button_pushed = false;


void setup() {
 
  Serial.begin(115200);
  delay(4000);
  Serial.print("Det funker din løk");
  Serial.println("\nOrientation Sensor real world accel output");

  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
   mpu_setup();

  start_time = millis();   
  pinMode(BUTTON_PIN, INPUT);
   
} 

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void ICACHE_RAM_ATTR dmpDataReady() {
    mpuInterrupt = true;
}

volatile bool sendPOST = false;

void mpu_setup()
{
  // join I2C bus (I2Cdev library doesn't do this automatically)
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin(21 , 22, 400000);
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
  #endif

  // initialize device
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);

  // verify connection
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // load and configure the DMP
  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();

  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // Calibration Time: generate offsets and calibrate our MPU6050
    mpu.CalibrateAccel(6);
    mpu.CalibrateGyro(6);
    mpu.PrintActiveOffsets();
    // turn on the DMP, now that it's ready
    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
}


void mpu_loop()
{
  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // wait for MPU interrupt or extra packet(s) available
  if (!mpuInterrupt && fifoCount < packetSize) return;

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;
    



    // display initial world-frame acceleration, adjusted to remove gravity
    // and rotated based on known orientation from quaternion
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
    Serial.print("aworld\t");
    Serial.print(aaWorld.x);
    Serial.print("\t");
    Serial.print(aaWorld.y);
    Serial.print("\t");
    Serial.print(aaWorld.z);
    Serial.print("\t");
    Serial.println(millis());
      if(button_pushed){
         Serial.print("Knapp er trykt");
        x_acc[num_of_measurements] = aaWorld.x;
        y_acc[num_of_measurements] = aaWorld.y;
        z_acc[num_of_measurements] = aaWorld.z;
        time_since_start[num_of_measurements] = millis()-start_time;
        num_of_measurements += 1;
        }
      
      
    
    //sendPOST = true; 
  }
}

 
void loop() {
 
 HTTPClient http;

 
    http.collectHeaders(headerKeysGET, numberOfHeadersGET); 
    // Your Domain name with URL path or IP address with path
    http.begin("https://gruppe11.innovasjon.ed.ntnu.no/nettside_app/sensor/");


if (!first_connect){
  int httpCode = http.GET();
     if (httpCode == 200) { //Check for the returning code
 
        String payload = http.getString();
        Serial.println(httpCode);
        Serial.println(payload);

        //=========X-CSRFTOKEN==================
        cookies += String(http.header("Cookies"));


        
         for(int i = 0 ; i < http.headers() ; i++){
          Serial.println(http.header(i));
           cookies=http.header(i);
          Serial.println("HER!!!!!");
         } 
         
        Serial.print("cookies check:");
        Serial.println(cookies);
        x_CSRFToken=cookies.substring(10, 74);
        cookies=cookies.substring(0, 74);
        Serial.print("X-CSRFToken check:");
        Serial.println(x_CSRFToken);
        Serial.print("cookies check:");
        Serial.println(cookies);
       
      }
 
    else {
      Serial.println("Error on HTTP request");
    }  
    first_connect = true;
  }

  //======================KJØRER MPU=====================
  mpu_loop();

  button_pushed = digitalRead(BUTTON_PIN);
  //============HER VELGER DU ANTALL MÅLINGER
if(num_of_measurements == 10 && !sendPOST){
  for(int i = 0; i < num_of_measurements; i++){
    sensorData[0] += String(x_acc[i]) +",";
     sensorData[1] += String(y_acc[i]) +",";
      sensorData[2] += String(x_acc[i]) +",";
       //sensorData[3] += time_since_start[i] +","
    }
    //Fjerner siste komma i hver streng
    sensorData[0].remove(-1);
    sensorData[1].remove(-1);
    sensorData[2].remove(-1);

    
     Serial.print("DATA CHECK:");
     Serial.print(sensorData[0]);
    Serial.print("&&");
    Serial.print(sensorData[1]);
    Serial.print("&&");
    Serial.println(sensorData[2]);
    sendPOST = true;
  }



  if (sendPOST){
    String postData= "";
    Serial.print("numberOfHeadersPOST: ");
    Serial.println(numberOfHeadersPOST); 
    Serial.println("for-loop");
            
       for ( int fieldNumber = 0; fieldNumber < numberOfHeadersPOST; fieldNumber++ ){
          Serial.print("fieldNumber: ");
          Serial.println(fieldNumber);
          String fieldName = headerKeysPOST[fieldNumber];
          String fieldData =  sensorData[fieldNumber];
          Serial.print("fieldName: ");
          Serial.println(fieldName);
          Serial.print("fieldData: ");
          Serial.println(fieldData);

          postData += fieldName + "=" + fieldData;
          if (fieldNumber+1!=numberOfHeadersPOST) {
           postData += "&";
          }
        }
        Serial.print(postData);


    //=============HER POSTER VI==================
    
    http.addHeader("Content-Type", "application/json");
    http.addHeader("X-CSRFToken", x_CSRFToken); 
    http.addHeader("referer", "https://gruppe11.innovasjon.ed.ntnu.no");
    http.addHeader("Cookie", cookies);

    
    int httpCode = http.POST(postData);
    int httpResponseCode = http.POST(postData);
    Serial.print("httpResponseCode: ");  
    Serial.println(httpResponseCode); 
    

     button_pushed = false;
  }
}
